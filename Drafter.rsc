%dimens.fic
*OFFSET
100.0              /* retrait ligne de cote (en % largeur std)
*TERMINATORS
1                  /* style fleche standard pour cotes lineaires et radiales
1                  /* description style fleche
-200               /* retrait extremite ligne de cote
3                  /* nb segements de la fleche en % de la largeur std
-200 -60 -200  60
-200  60    0   0
   0   0 -200 -60
*END
%eia.fic
 -1 /*^ 1
 -1 /*^ 2
 -1 /*^ 3
 -1 /*^ 4
 -1 /*^ 5
 -1 /*^ 6
 -1 /*^ 7
 -1 /*^ 8
 -1 /*^ 9
 -1 /*^10
 -1 /*^11
 -1 /*^12
 -1 /*^13
 -1 /*^14
 -1 /*^15
 -1 /*^16
 -1 /*^17
 -1 /*^18
 -1 /*^19
 -1 /*^20
 -1 /*^21
 -1 /*^22
 -1 /*^23
 -1 /*^24
 -1 /*^25
 -1 /*^26
 -1 /*^27
 -1 /*^28
 -1 /*^29
 -1 /*^30
 -1 /*^31
  7 /*
 -1 /* !
  7 /* "
 -1 /* #
 -1 /* $
  7 /* %
  7 /* &
 -1 /* '
  2 /* (
  3 /* )
 -1 /* *
  5 /* +
 -1 /* ,
  6 /* -
  4 /* .
 -1 /* /
  0 /* 0
  0 /* 1
  0 /* 2
  0 /* 3
  0 /* 4
  0 /* 5
  0 /* 6
  0 /* 7
  0 /* 8
  0 /* 9
 -1 /* :
 -1 /* ;
 -1 /* <
 -1 /* =
 -1 /* >
 -1 /* ?
 -1 /* @
 -1 /* A
 -1 /* B
 -1 /* C
 -1 /* D
 -1 /* E
 -1 /* F
102 /* G
114 /* H
206 /* I
207 /* J
 -1 /* K
 -1 /* L
103 /* M
101 /* N
 -1 /* O
 -1 /* P
 -1 /* Q
208 /* R
 -1 /* S
110 /* T
111 /* U
112 /* V
113 /* W
204 /* X
205 /* Y
 -1 /* Z
 -1 /* [
 -1 /* \
 -1 /* ]
 -1 /* ^
 -1 /* _
 -1 /* `
 -1 /* a
 -1 /* b
 -1 /* c
 -1 /* d
 -1 /* e
 -1 /* f
 -1 /* g
 -1 /* h
 -1 /* i
 -1 /* j
 -1 /* k
 -1 /* l
 -1 /* m
 -1 /* n
 -1 /* o
 -1 /* p
 -1 /* q
 -1 /* r
 -1 /* s
 -1 /* t
 -1 /* u
 -1 /* v
 -1 /* w
 -1 /* x
 -1 /* y
 -1 /* z
 -1 /* {
 -1 /* |
 -1 /* }
 -1 /* ~
 -1 /* 
2          /* liste des departs arrets usinages
 7 8 2  
 155 156 1
0          /* 0 absolu 1 relatif pour X et Y
103        /* registre usinage (en general 103=M)
0          /* 1 BYSTRONIC (suppression du premier element), 0 sinon
1          /* essi=0, eia=1
1          /* unite d entree ( 1.=mm, 0.1=dixieme )
0          /* distmin (en fonction seulement dans les versions bys)
1          /* 0 absolu 1 relatif pour I et J (+10 change avec x et y)
90         /* code pour passage a absolu
91         /* code pour passage a relatif
%igs.fic
-998 -998 -998 -998  /* coul/layer(fin=-999/-998),usinage,ss-usinage,outil
1 2 -1 -1
2 2 -1 -1
3 1 -1 -1
-998 -999 -999 -999
%interf.fic
0             /0 pas, 1 part.list par directory de pieces, 2 un seul part.l ist
1             /1 ligne avec saignee et outil en 1/10mm, 2 saignee et outil mais en unite du fichier
1             /1 angle en degre 0 en radian
0             /1 (0,0) au milieu du rectangle exinscrit, 2 ecriture du decalage ds .ref
0             /code usinage 0 coupe, 1 marquage, 2 poincon; si 1  01 02 marq faces A  B, 11,12 coupe A et B, 21 22 percage
%me10.fic
/USI    /* correspondance usinage dpr --> couleur trait me10
2
3 0
2
7 6
2
7 0
0
0
%mqimaje.fic
4    /* nbre de polices : Style, haut (en 10ieme), nb ligne, chaines en 64 (nbligne * nbcar < 256)
 1   80  2
MMMMMMMMMMMMMMMM
MMMMMMMMMMMMMMMM
 1   45  2
MMMMMMMMMMMMMMMM
MMMMMMMMMMMMMMMM
 1   25  2
MMMMMMMMMMMMMMMM
MMMMMMMMMMMMMMMM
 1   15  2
MMMMMMMMMMMMMMMM
MMMMMMMMMMMMMMMM
5             /* nbre de division du rect du reper pr algo de plac
100           /* en 1/10mm ecart entre bord piece et mq chanfrein
150 150       /* en 1/10mm tailles marques debut et fin de chanfr
9 200         /* nb essais de deplacement // a elt et valeur du dep
9 150         /* nb essais de dep per a elt et val du dep
40            /* ecart max entre cercle et polyg
0.7           /* longueur max en m pr rassembler 2 elts ayant meme chfr
40            /* idem mais pr les angles en deg entre deb d'elt1 et fin elt2
40            /* idem mais pr les angles<0 en deg entre deb d'elt1 et fin elt2
4 5 4         /* usi vrai,usi faux,usi trait
0 0           /* 0 relecture de ce fichier 1 une seule lecture
50            /* i1 Depassemnt du segment en mm
0             /* i2 Recherche autour du centre de gravite
3             /* i3 1 !chf | 2 !repere | 3 sinon
1             /* i4 Sous-usinage
0             /* i5 Si <> 0 Angle au-dela duquel il n'y a plus de mq de chf
0             /* i6 Pourcentage de marge autour du texte du repere
0             /* i7
0             /* i8
0             /* i9
%
